#!/bin/bash

CODE=0

# Perform style check in src folder
pycodestyle src/ --max-line-length=120 --ignore=E701,E226
RESULT=$?
CODE=$((CODE + RESULT))

# Perform style check in bin folder
pycodestyle bin/* --max-line-length=120 --ignore=E701,E226,E402
RESULT=$?
CODE=$((CODE + RESULT))


# Perform style check in tests folder
pycodestyle tests/ --max-line-length=120 --ignore=E701,E226
RESULT=$?
CODE=$((CODE + RESULT))

if [ "$CODE" -eq "0" ]; then
  echo "All style checks passed. ;)";
else
  echo "Code style issues detected, please fix errors above. :(";
fi
exit $CODE