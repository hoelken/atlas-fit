import numpy as np
from src.atlas_fit.base.config import *


def test_fwhm_computation():
    expectation = 8.485964045000001e-05
    wl = 411.97

    np.testing.assert_almost_equal(Atlas().compute_fwhm(wl), expectation)


def test_creation_from_dict():
    setup = {
        'label': 'Test',
        'input': {'corrected_frame': 'foo/bar.baz'},
        'atlas': {'line_window': 5}
    }
    config = Config.from_dict(setup.copy())

    assert config.label == setup['label']
    assert config.input.corrected_frame == setup['input']['corrected_frame']
    assert config.atlas.key == 'fts'
    assert config.atlas.line_window == setup['atlas']['line_window']
    assert config.fitting.continuum_points == 100


def test_creation_from_dict_with_optional():
    setup = {
        'label': 'Test',
        'input': {'corrected_frame': 'foo/bar.baz'},
        'atlas': {'line_prominence': 5},
        'fitting': {'continuum_points': 42}
    }
    config = Config.from_dict(setup.copy())

    assert config.fitting.continuum_points == 42


def test_repr():
    rep = repr(Config())
    assert 'label' in rep
    assert 'Data' in rep
    assert 'Atlas' in rep
