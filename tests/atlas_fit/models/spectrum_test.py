import numpy as np

from src.atlas_fit.models.spectrum import Spectrum


def test_straighten():
    poly = np.polynomial.Polynomial.fromroots([5, 16])
    s = Spectrum(poly(np.arange(1000)), [10, 100, 500], 6)
    s.straighten()
    np.testing.assert_almost_equal(1, 1, decimal=2)
