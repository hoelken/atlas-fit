import numpy as np

from src.atlas_fit.models.differential_convolution import FWHM, DifferentialSpectralPSF, FWHM2SIGMA
from src.atlas_fit.utils.fitting_functions import gaussian


def test_spsf_generation():
    xes = np.arange(100)
    sigma = 5/FWHM2SIGMA
    gauss = gaussian(xes, mean=50, sigma=sigma, amplitude=1)
    gauss = gaussian(xes, mean=50, sigma=sigma, amplitude=1/np.sum(gauss))
    dspsf = DifferentialSpectralPSF(FWHM(1, 6))
    dspsf._gen_convolution_function(xes)
    assert dspsf.fwhm.delta == 5.
    np.testing.assert_array_almost_equal(gauss, dspsf._delta_spsf)
