import numpy as np

from src.atlas_fit.atlantes import atlas_factory
from src.atlas_fit.base.config import Config
from src.atlas_fit.base.constants import NM2ANGSTROM
from src.atlas_fit.comperator import Comperator
from src.atlas_fit.utils.tailoring import select_best_atlas_range, select_data_fit_region


def test_select_atlas_region():
    conf = Config()
    conf.atlas.key = 'hhdc'
    c = Comperator(np.arange(8000), conf, {'data': [], 'atlas': []})
    c.atlas = atlas_factory(conf.atlas.key, 450, 455, conversion=NM2ANGSTROM)
    c.dispersion = np.polynomial.Polynomial.fit([0, 8000 - 1], [0, len(c.atlas.wl)], 1)
    atlas = select_best_atlas_range(c, conf)
    resolution = np.mean(atlas.wl[1:] - atlas.wl[:-1])
    assert np.abs(atlas.wl.min() - 450) <= 2 * resolution
    assert np.abs(atlas.wl.max() - 455) <= 2 * resolution


def test_select_data_region():
    conf = Config()
    conf.atlas.key = 'hhdc'
    c = Comperator(np.arange(8000), conf, {'data': [], 'atlas': []})
    c.atlas = atlas_factory(conf.atlas.key, 450, 455, conversion=NM2ANGSTROM)
    c.dispersion = np.polynomial.Polynomial.fit([0, 8000 - 1], [0, len(c.atlas.wl)], 1)
    atlas = atlas_factory(conf.atlas.key, 450.5, 454.5, conversion=NM2ANGSTROM)

    atlas_resolution = np.mean(c.atlas.wl[1:] - c.atlas.wl[:-1])
    data_resolution = atlas_resolution * len(c.atlas.wl) / len(c.spectrum.data)

    minx, maxx = select_data_fit_region(c, atlas)
    assert len(c.spectrum.data) - (maxx - minx) < 1.01 / data_resolution
