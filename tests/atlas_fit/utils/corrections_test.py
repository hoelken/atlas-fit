import numpy as np

from src.atlas_fit.base.config import Fitting
from src.atlas_fit.utils.corrections import prepare_spectrum


def test_spectrum_preparation(mocker):
    mock = mocker.patch('src.atlas_fit.utils.corrections.Spectrum')
    instance = mock.return_value
    conf = Fitting()
    data = np.arange(10)
    instance.data = data
    result = prepare_spectrum(data, conf, target_mean=1, error_correction=np.roll(data, 5))

    assert mock.called_with(data)
    instance.apply_lowpass_filter_correction.assert_called_with(conf)
    instance.normalize.assert_called_with(1)
    assert instance.straighten.called
    assert result == instance
