from src.atlas_fit.utils.utils import *


def test_parse_shape():
    assert (slice(None, None), None) == parse_shape('[:,]')
    assert (1, slice(2, 3)) == parse_shape('[1,2:3]')
    assert (1, slice(2, 3)) == parse_shape('1,2:3')
    assert (1, slice(2, 3), slice(4, 5)) == parse_shape('1,2:3,4:5')
    assert (1, slice(2, 3), slice(4, 5), 6) == parse_shape('1,2:3,4:5,6')


def test_to_slice():
    assert slice(None, None) == to_slice('[:]')
    assert slice(None, None) == to_slice(':')
    assert slice(42, 1337) == to_slice('42 : 1337')


def test_to_int():
    assert 1 == to_int('1')
    assert 0 == to_int('0')
    assert 1337 == to_int(' 1337 ')
    assert to_int('abc') is None
    assert to_int('9d') is None
    assert to_int('0x9') is None
