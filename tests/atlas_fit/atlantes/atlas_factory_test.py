import pytest
from src.atlas_fit.atlantes import atlas_factory


def test_factory():
    assert atlas_factory('hhdc', 5249, 5251) is not None
    assert atlas_factory('hhavrg', 5249, 5251) is not None
    assert atlas_factory('sss', 5249, 5251) is not None


def test_not_implemented():
    with pytest.raises(NotImplementedError):
        atlas_factory('foobar', 5249, 5251)
