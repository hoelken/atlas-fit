#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for hamburg atlas

@author: hoelken
"""
import os.path
import pytest
import numpy as np

from src.atlas_fit.atlantes import FTSDCAtlas
from src.atlas_fit.base.errors import IllegalStateError


def test_load_values():
    wl_min = 3999.0
    wl_max = 4001.0
    atlas = _fts_atlas(wl_min, wl_max).load()
    assert 21 == len(atlas)
    assert wl_min == atlas.wl.min()
    assert wl_max == atlas.wl.max()
    assert 0.300062 == atlas.intensity.min()
    assert 0.32126 == atlas.intensity.max()


def test_does_not_load_twice():
    atlas = _fts_atlas(0, 3999).load()
    assert 4 == len(atlas)
    atlas.load()
    assert 4 == len(atlas)


def test_custom_fields():
    atlas = _fts_atlas(0, 3999).load()
    assert 0 != atlas.continuum.size


def test_does_raise_if_not_found():
    with pytest.raises(IllegalStateError):
        FTSDCAtlas(0, 1, '/does/not/exist').load()


def test_wl_conversion():
    factor = 1/10
    wl_min = 3999.0
    wl_max = 3999.3
    atlas = _fts_atlas(wl_min, wl_max).load()
    atlas.convert_wl(factor)
    assert wl_min * factor == atlas.wl.min()
    assert wl_max * factor == atlas.wl.max()


def _fts_atlas(start, stop):
    return FTSDCAtlas(start, stop, os.path.join('tests', 'resources', 'atlantes'))
