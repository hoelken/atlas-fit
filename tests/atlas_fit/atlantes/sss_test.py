#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for SSS atlas

@author: hoelken
"""
import os.path
import pytest

from src.atlas_fit.atlantes.sss import SSSAtlas
from src.atlas_fit.base.errors import IllegalStateError


def test_load_values():
    wl_min = 3161.0
    wl_max = 3166.0
    atlas = _sss_atlas(wl_min, wl_max).load()
    assert 9 == len(atlas)
    assert wl_min == atlas.wl.min()
    assert wl_max == atlas.wl.max()
    assert 0.23276 == atlas.intensity.min()
    assert 0.25676 == atlas.intensity.max()


def test_does_not_load_twice():
    atlas = _sss_atlas(0, 3161.5).load()
    assert 2 == len(atlas)
    atlas.load()
    assert 2 == len(atlas)


def test_custom_fields():
    atlas = _sss_atlas(0, 3161.5).load()
    assert 0 != atlas.stokes_q.size
    assert 0 != atlas.stokes_q_smoothed.size
    assert 0 != atlas.continuum.size


def test_does_raise_if_not_found():
    with pytest.raises(IllegalStateError):
        SSSAtlas(0, 1, '/does/not/exist').load()


def test_wl_conversion():
    factor = 1/10
    wl_min = 3161.0
    wl_max = 3161.5
    atlas = _sss_atlas(wl_min, wl_max).load()
    atlas.convert_wl(factor)
    assert wl_min * factor == atlas.wl.min()
    assert wl_max * factor == atlas.wl.max()


def _sss_atlas(start, stop):
    return SSSAtlas(start, stop, os.path.join('tests', 'resources', 'atlantes'))
