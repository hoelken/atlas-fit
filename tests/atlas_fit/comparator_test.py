from unittest.mock import MagicMock

import numpy as np

from src.atlas_fit.base.config import Config
from src.atlas_fit.comperator import Comperator


def test_preparation(mocker):
    mocker.patch('src.atlas_fit.comperator.Spectrum')
    c = Comperator(None, Config(), {'data': None})

    c._prepare_spectrum()
    assert c.spectrum.prepare.called


def test_atlas_loading(mocker):
    mocker.patch('src.atlas_fit.comperator.Spectrum')
    mock_factory = mocker.patch('src.atlas_fit.comperator.atlas_factory')
    conf = Config()
    conf.input.wl_start = 123
    conf.input.wl_end = 456
    conf.atlas.key = 'TEST'
    conf.atlas.fwhm = 0.1
    c = Comperator(None, conf, {'data': None})

    c._load_atlas()
    mock_factory.assert_called_with("TEST", 123, 456, conversion=10)


def test_atlas_line_fitting(mocker):
    spectrum_constructor = mocker.patch('src.atlas_fit.comperator.Spectrum')
    atlas = MagicMock()
    conf = Config()
    conf.atlas.line_window = 5
    c = Comperator(None, conf, {'data': None, 'atlas': [1, 5, 19]})
    c.atlas = atlas
    atlas.wl = np.arange(22)
    atlas.intensity = np.ones(22)

    c._find_lines_in_atlas()
    spectrum_constructor.assert_called_with(atlas.intensity, [1, 5, 19], 5)
    assert spectrum_constructor.return_value.fit_lines.called


def test_no_error(mocker):
    mocker.patch('src.atlas_fit.comperator.Spectrum')
    c = Comperator(None, Config(), {'data': None})
    c._ref = np.ones(5)
    assert c._error_without_masked_lines(np.ones(5)) == 0


def test_full_error(mocker):
    mocker.patch('src.atlas_fit.comperator.Spectrum')
    c = Comperator(None, Config(), {'data': None})
    c._ref = np.ones(5)
    assert c._error_without_masked_lines(np.zeros(5)) == 5


def test_error_ignore_line(mocker):
    mocker.patch('src.atlas_fit.comperator.Spectrum')
    conf = Config()
    conf.input.line_window = 2
    conf.fitting.stray_light_ignored_lines = [0]
    c = Comperator(None, conf, {'data': None})
    c._wl = np.arange(5)
    c._ref = np.ones(5)
    assert c._error_without_masked_lines(np.zeros(5)) == 4.5


def test_data_continuum_correction(mocker):
    mocker.patch('src.atlas_fit.comperator.Spectrum')
    c = Comperator(None, Config(), {'data': None})
    c.atlas = MagicMock()
    c.atlas.intensity = np.ones(22)
    c.atlas.continuum = np.ones(22) * 2
    c.asp = MagicMock()
    c.asp.data = np.ones(22)
    c.asp.fitted_continuum.return_value = np.ones(22)

    c._correct_data_continuum()
    assert c.spectrum.normalize.called
    assert c.spectrum.straighten.called


def test_find_fit(mocker):
    mocker.patch('src.atlas_fit.comperator.Spectrum')
    brute = mocker.patch('src.atlas_fit.comperator.brute')
    brute.return_value = 1
    c = Comperator(None, Config(), {'data': None})
    c._peaks_sp = [1, 2, 3]
    c._peaks_asp = [2, 3, 4]

    c._find_fit()
    assert brute.called
    assert c.dispersion is not None
    assert c.deg == 1
