# Supported atlantes

## Hamburg Atlas (FTS Atlas)
The folder `FTS-Atlas` may contain the files `file01` to `file10` of the
[Hamburg Spectral Atlas (Neckel et. al, 1999)](https://ui.adsabs.harvard.edu/abs/1999SoPh..184..421N/abstract)
for the disk-averaged spectrum. The columns are 

1. wavelength,
2. spectral intensity and 
3. (quasi-)continuum.

Follow the instructions in the referenced publication
to download the files from the [FTP Server](ftp://ftp.hs.uni-hamburg.de/pub/outgolng/FTS-Atlas).

You can load the content with the classes and methods
provided by `atlas_fit.hamburg` module.

## Second Solar Spectrum IRSOL (SSS Atlas)
The folder `SSS-Atlas` may contain the file `SSSatlas.txt`. An ASCII encoded
txt file with the [Second Solar Spectrum Atlas](https://www.irsol.usi.ch/data-archive/second-solar-spectrum-ss2-atlas/)
provided in electronic form by IRSOL as a compilation by Stenflo (2014),
based on the atlas of Gandorfer (2000, 2002, 2005).

Rows 0 to 3 are ignored as header. Columns in the file should be ordered as follows:

| #   | Name   | Description                 | Type   | Comment                    |
|-----|--------|-----------------------------|--------|----------------------------|
| 0   | **w**  | Wavelength                  | double | 3161.2 – 6987.0 Å          |
| 1   | **b**  | I/I_c                       | double | µ = 0.10.                  |
| 2   | **p**  | Q/I (%)                     | double | µ = 0.10, unsmoothed       |
| 3   | **pw** | Q/I (%)                     | double | µ = 0.10, wavelet smoothed |
| 4   | **pc** | Empirical continuum Q/I (%) | double | µ = 0.10.                  |

You can load the content with the classes and methods
provided by `atlas_fit.sss` module.