import numpy as np
from scipy.ndimage import zoom
from scipy.signal import resample
from scipy.optimize import minimize, OptimizeResult

from ..base.config import Config
from ..atlantes import Atlas
from ..models.differential_convolution import FWHM, DifferentialSpectralPSF


class ParameterFit:

    def __init__(self, spectrum: np.array, atlas: Atlas, config: Config, border: int = 30):
        self._original_data = spectrum
        self._original_atlas = atlas
        self.data = None
        self.atlas = None
        self.stray_light = None
        self.fwhm = None
        self.ignored_regions = []
        self._conf = config
        self._border = border

    def minimize_delta(self, stray_light: float, fwhm: float, bounds: tuple = None) -> OptimizeResult:
        self._set_ignored_regions()
        max_straylight = self._conf.fitting.max_stray_light/100
        max_fwhm = self._conf.fitting.max_fwhm
        bounds = ((0.0, max_straylight), (0, max_fwhm)) if bounds is None else bounds
        return minimize(self._compare, (stray_light, fwhm), bounds=bounds)

    def _set_ignored_regions(self):
        wl = zoom(self._original_atlas.wl, len(self._original_data) / len(self._original_atlas.wl),
                  order=4, mode='nearest', grid_mode=True)
        win = (self._conf.input.line_window + 4) // 2
        for line in self._conf.fitting.stray_light_ignored_lines:
            pos = np.argmin(np.abs(wl - float(line)))
            self.ignored_regions.append(slice(max(0, pos - win), min(pos + win, len(wl))))

    def _compare(self, params: tuple) -> np.array:
        # params[0]: stray-light in percent
        # params[1]: FWHM in nm
        self._set_current_params(params[0], params[1])
        self._apply_stray_light()
        self._convolute_atlas()
        self._adjust_scales()
        return self.global_error()

    def _set_current_params(self, stray_light: float, fwhm: float):
        self.stray_light = stray_light
        self.fwhm = fwhm

    def _apply_stray_light(self):
        self.data = self._original_data - (self._original_data.mean() * self.stray_light)
        self.data = (self.data / self.data.mean()) * self._original_atlas.intensity.mean()

    def _convolute_atlas(self):
        if self.fwhm == 0:
            self.atlas = self._original_atlas.intensity
        else:
            fwhm = FWHM(atlas=self._conf.atlas.fwhm, data=self.fwhm)
            self.atlas = DifferentialSpectralPSF(fwhm).convolve(self._original_atlas.intensity, self._original_atlas.wl)

    def _adjust_scales(self):
        self.atlas = resample(self.atlas, len(self.data))
        if self._border > 0:
            self.atlas = self.atlas[self._border:-self._border]
            self.data = self.data[self._border:-self._border]

    def compute_error(self):
        return (self.data - self.atlas) ** 2 / self.data ** 2

    def global_error(self):
        error = self.compute_error()
        for r in self.ignored_regions:
            error[r] = 0
        return error.sum()
