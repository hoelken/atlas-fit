import logging
import numpy as np


from ..base.config import Fitting
from ..atlantes import Atlas
from ..models import Spectrum

log = logging.getLogger()


def compute_continuum_fit_error(atlas: Atlas, config: Fitting) -> np.array:
    atlas_intensity = Spectrum(atlas.intensity)
    atlas_intensity.fit_continuum(config)
    yes = atlas_intensity.fitted_continuum(np.arange(len(atlas)))
    cont = atlas.continuum
    if cont.min() < atlas.intensity.max():
        log.warning('Adjusted continuum level from %.2f to %.2f', cont.mean(), cont.mean() + atlas.intensity.max())
        log.warning('Flux estimation is now invalid!!')
        cont += atlas.intensity.max()
    return atlas.continuum - yes


def prepare_spectrum(data: np.array, config: Fitting,
                     target_mean: float, error_correction: np.array) -> Spectrum:
    s = Spectrum(data)
    s.apply_lowpass_filter_correction(config)
    s.normalize(target_mean)
    s.straighten(config, error_correction * (s.data.mean() / target_mean))
    return s
